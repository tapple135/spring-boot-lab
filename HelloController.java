
package com.demo.microservices.controller;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.microservices.model.Hello;

@RestController
public class HelloController {
	private String msgTemplate = "%s 님 반갑습니다.";
	private final AtomicLong vistorCounter = new AtomicLong();


	@GetMapping("/hello")
	public Hello getHelloMsg(@RequestParam(value="name") String name) {
		
		return new Hello(vistorCounter.incrementAndGet(), String.format(msgTemplate, name));
	}
}